#include "Ribosome.h"
#include <iostream>
#include <string>

/*
int main(void)
{
	Protein* aminoList;
	Ribosome ri;
	std::string rnaExample = "UUACUUUUG";
	aminoList = ri.create_protein(rnaExample);
	return 0;
}
*/

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* aminoList = new Protein();
	AminoAcidNode* head = new AminoAcidNode();
	aminoList->init();
	aminoList->set_first(head);
	std::string rnaStr = RNA_transcript;
	std::string aminoCodon = "";
	int subStart = 0;
	int subCount = 3;
	int rnaLength = 0;

	do
	{
		rnaLength = rnaStr.length();
		aminoCodon = rnaStr.substr(subStart, subCount);
		if (get_amino_acid(aminoCodon) == UNKNOWN)
		{
			aminoList->clear();
		}
		else
		{
			aminoList->add(get_amino_acid(aminoCodon));
		}
		rnaStr = rnaStr.substr(subCount - 1, rnaLength - 3);
	} while (rnaStr[subCount - 1] && get_amino_acid(aminoCodon) != UNKNOWN);
	delete(head);

	return aminoList;
}