#include "Nucleus.h"
#include <iostream>
#include <string>
#include <algorithm>

void Gene::init(unsigned int start, unsigned int end, bool on_complementary_dna_strand)
{
	this->start = start;
	this->end = end;
	this->on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::getStart() const
{
	return this->start;
}

unsigned int Gene::getEnd() const
{
	return this->end;
}

bool Gene::get_on_complementary_dna_strand() const
{
	return this->on_complementary_dna_strand;
}

void Gene::setStart(const unsigned int newstart)
{
	this->start = newstart;
}

void Gene::setEnd(const unsigned int newend)
{
	this->end = newend;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->on_complementary_dna_strand = on_complementary_dna_strand;
}

void Nucleus::init(const std::string dna_sequence)
{
	std::string temp = "";
	int i = 0;
	std::string compleDnaStr = "";
	int strLength = 0;
	strLength = dna_sequence.length();
	for (i = 0; i < strLength; i++)
	{
		if (dna_sequence[i] == 'A')
		{
			temp = "T";
			compleDnaStr = compleDnaStr + temp;
		}
		else if (dna_sequence[i] == 'T')
		{
			temp = "A";
			compleDnaStr = compleDnaStr + temp;
		}
		else if (dna_sequence[i] == 'C')
		{
			temp = "G";
			compleDnaStr = compleDnaStr + temp;
		}
		else if (dna_sequence[i] == 'G')
		{
			temp = "C";
			compleDnaStr = compleDnaStr + temp;
		}
		else
		{
			std::cerr << "There is a problem with the D.N.A sequence that you entered!";
			_exit(1);
		}
	}
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = 0;
	int dnaLength = 0;
	Nucleus dnaClass;
	std::string rnaStr = "";
	std::string temp = "";
	bool complTOrF = gene.get_on_complementary_dna_strand();
	unsigned int dnaStart = gene.getStart();
	unsigned int dnaEnd = gene.getEnd();

	if (complTOrF)
	{
		dnaLength = dnaClass.complementary_DNA_strand.length();
		for (i = 0; i < dnaLength; i++)
		{
			if (i >= int(dnaStart) && i <= int(dnaEnd))
			{
				temp = dnaClass.complementary_DNA_strand[i];
				if (dnaClass.complementary_DNA_strand[i] == 'T')
				{
					temp = "U";
				}
				rnaStr = rnaStr + temp;
			}
		}
	}
	else
	{
		dnaLength = dnaClass.DNA_strand.length();
		for (i = 0; i < dnaLength; i++)
		{
			if (i >= int(dnaStart) && i <= int(dnaEnd))
			{
				temp = dnaClass.DNA_strand[i];
				if (dnaClass.DNA_strand[i] == 'T')
				{
					temp = "U";
				}
				rnaStr = rnaStr + temp;
			}
		}
	}

	return rnaStr;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	int i = 0;
	Nucleus dnaClass;
	std::string reversedStr = dnaClass.DNA_strand;
	int strLength = 0;
	strLength = reversedStr.length();
	for (i = 0; i < strLength / 2; i++)
	{
		std::swap(reversedStr[i], reversedStr[strLength - i - 1]);
	}

	return reversedStr;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon)
{
	Nucleus dnaClass;
	int numberOfApp = 0;
	numberOfApp = std::count(dnaClass.DNA_strand.begin(), dnaClass.DNA_strand.end(), codon);

	return numberOfApp;
}