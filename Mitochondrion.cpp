#include <iostream>
#include <string>
#include "Mitochondrion.h"

// The function init all it's private variables.
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

// The function check if the amino list is the right one.
void Mitochondrion::insert_glocuse_receptor(const Protein& protein)
{

}

// The function set the glocuse level.
void Mitochondrion::set_glocuse(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

// The function check if the mitochondrion has glocuse receptor and return the answer.
bool Mitochondrion::produceATP() const
{
	bool canCreateATP = false;
	if (this->_has_glocuse_receptor == true)
	{
		canCreateATP = true;
	}
	return canCreateATP;
}