#pragma once
#include <string>
#include <iostream>

class Gene
{
private:
	unsigned int start;
	unsigned int end;
	bool on_complementary_dna_strand;
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	unsigned int getStart() const;
	unsigned int getEnd() const;
	bool get_on_complementary_dna_strand() const;
	void setStart(const unsigned int newstart);
	void setEnd(const unsigned int newend);
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
};

class Nucleus
{
private:
	std::string DNA_strand;
	std::string complementary_DNA_strand;
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon);
};