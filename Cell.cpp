#include <iostream>
#include <string>
#include "Cell.h"

// The function init all the private variables.
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;

}

// The function check if the cell can be charged with energy and return the answer.
bool Cell::get_ATP()
{
	Protein* myAmino;
	std:: string rnaStr = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	myAmino = this->_ribosome.create_protein(rnaStr);
	this->_mitochondrion.insert_glocuse_receptor(*myAmino);
	bool atp = this->_mitochondrion.produceATP();

	return atp;
}