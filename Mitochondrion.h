#pragma once
#include <iostream>
#include "Protein.h"

class Mitochondrion
{
	private:
		unsigned int _glocuse_level;
		bool _has_glocuse_receptor;

	public:
		void init();
		void insert_glocuse_receptor(const Protein & protein);
		void set_glocuse(const unsigned int glocuse_units);
		bool produceATP() const;
};